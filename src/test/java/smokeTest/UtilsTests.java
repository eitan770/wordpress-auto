package smokeTest;

import org.testng.Assert;
import org.testng.annotations.Test;
import utils.DateTool;

import java.text.ParseException;

public class UtilsTests {
    @Test
    public void dateFormatTest() throws ParseException {
        String dateToFormat = "11/10/19";
        String dateExpectedFormat = "10-11-2019";
        String actualResult = DateTool.convertDateFormat("11/10/19", "DD/mm/yy", "mm-DD-yyyy");
        Assert.assertEquals(dateExpectedFormat, actualResult);
    }
}
