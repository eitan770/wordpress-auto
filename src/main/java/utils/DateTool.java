package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTool {

    public static final Logger logger = LoggerFactory.getLogger(DateTool.class);



        public static String convertDateFormat (String date, String initialFormat, String requiredFormat) throws
        ParseException {
        logger.info ("Converting date {} torequired format {}",date, requiredFormat);
        logger.debug ("Converting date {} torequired format {}",date, requiredFormat);
                Date dateToConvert = new SimpleDateFormat(initialFormat).parse(date);
        return new SimpleDateFormat(requiredFormat).format(dateToConvert);
    }

    public static void main(String[] args) throws ParseException {
        String dateFormat = convertDateFormat("11/10/19", "DD/mm/yy", "mm-DD-yyyy");
        System.out.println();

    }


    /*String formatedDate = "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(initialFormat);


    formatedDate = new SimpleDateFormat(requiredFormat).format(dateToConvert);


    return formatedDate*/
    }

